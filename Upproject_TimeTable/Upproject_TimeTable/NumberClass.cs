﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upproject_TimeTable
{

    public class NumberClass
    {
        public string NO;
        public string Subject;
        public string ClassNumber;
        public string divide;
        public string ClassName;
        public string division;
        public string grade;
        public string credit;
        public string time;
        public string room;
        public string professor;
        public string language;

        public void Output()
        {
            Console.WriteLine("  "+NO.PadRight(3)+"  " + Subject.PadRight(8) + "  " +ClassNumber.PadRight(6)+ "  "+divide.PadRight(3)+"  "+ClassName.PadRight(20)+ "  "+division.PadRight(4)+ "  "+grade.PadRight(1)+ "  "+credit.PadRight(3)+ "  "+time.PadRight(30)+ "  "+room.PadRight(7)+ "  "+professor.PadRight(26)+ "  "+language.PadRight(3));
            //Console.WriteLine("{0,5:NO}{1,-5:NO}{2,5:NO}{3,5:NO}{4,5:NO}{5,5:NO}{6,5:NO}{7,5:NO}{8,5:NO}{9,5:NO}{10,5:NO}{11,5:NO}", NO,Subject,ClassNumber,divide,ClassName,division,grade,credit,time,room,professor,language);// "  " + NO + "  " + Subject + "  " + ClassNumber + "  " + divide + "  " + ClassName + "  " + division + "  " + grade + "  " + credit + "  " + time + "  " + room + "  " + professor + "  " + language);
        }
        public void Output2()
        {
            Console.WriteLine("  " + Subject.PadRight(8) + "  " + ClassNumber.PadRight(6) + "  " + divide.PadRight(3) + "  " + ClassName.PadRight(20) + "  " + division.PadRight(4) + "  " + grade.PadRight(1) + "  " + credit.PadRight(3) + "  " + time.PadRight(30) + "  " + room.PadRight(7) + "  " + professor.PadRight(26) + "  " + language.PadRight(3));
            //Console.WriteLine("{0,5:NO}{1,-5:NO}{2,5:NO}{3,5:NO}{4,5:NO}{5,5:NO}{6,5:NO}{7,5:NO}{8,5:NO}{9,5:NO}{10,5:NO}{11,5:NO}", NO,Subject,ClassNumber,divide,ClassName,division,grade,credit,time,room,professor,language);// "  " + NO + "  " + Subject + "  " + ClassNumber + "  " + divide + "  " + ClassName + "  " + division + "  " + grade + "  " + credit + "  " + time + "  " + room + "  " + professor + "  " + language);
        }

        public string[] SplitOutput()
        {
            string[] DayTime = time.Split(Convert.ToChar(" "));
            return DayTime;
        }
    }
}
