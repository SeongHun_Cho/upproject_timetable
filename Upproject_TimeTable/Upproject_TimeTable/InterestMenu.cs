﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Upproject_TimeTable
{

    class InterestMenu
    {

        public Array ExcelData = new Array[300];
        public List<NumberClass> InterestSubject = new List<NumberClass>();
        double TotalGrade = 0;
        bool repeat = true;


        // 관심과목 목록
        public List<NumberClass> Interest(List<NumberClass> InterestSubject, Array ExcelData)
        {
            
            while (repeat)
            {
                this.InterestSubject = InterestSubject;
                this.ExcelData = ExcelData;
                Console.Write("       관    심    과    목\n\n\n\n");
                Console.Write("1. 관심과목 추가\n\n");
                Console.Write("2. 관심과목 삭제\n\n");
                Console.Write("3. 관심과목 조회\n\n");
                Console.Write("4. 전체강의 목록\n\n");
                Console.Write("5. 강의 검색 \n\n");
                Console.Write("6. 처음 메뉴로 돌아가기\n\n");
                Console.Write("7. 종료\n\n");
                Console.Write("원하는 번호를 고르시오. :");
                ConsoleKeyInfo Exam;
                Exam = Console.ReadKey();

                if (Exam.Key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    break;
                }


                else
                {
                    int.TryParse(Exam.KeyChar.ToString(), out int select);

                    switch (select)
                    {
                        case 1:
                            Console.Clear();
                            bool repeat4 = true;
                            while (repeat4)
                            {
                                InterestInput();
                                Console.Write("다시 신청하시겠습니까?     1.Yes 2.No\n");
                                int.TryParse(Console.ReadLine(), out int repeatcheck);
                                if (repeatcheck == 1)
                                {
                                    repeat4 = true;
                                }
                                else if (repeatcheck == 2)
                                {
                                    repeat4 = false;
                                    Console.Clear();
                                }
                            }
                            break;
                        case 2:
                            Console.Clear();
                            Title();
                            InterestCheck();
                            InterestRemove();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();//이거 돌아가는 함수는 없나???
                            Console.Write("Num    ");
                            Title();
                            //for(int i =0; i<Applysubject.Length;i++){Applysubject[i].Output();}
                            InterestCheck();
                            Console.Write("\n 총학점 :  ");
                            Console.Write(TotalGrade + "\n");
                            Console.Write("1번키를 누르면 돌아갑니다.");
                            int.TryParse(Console.ReadLine(), out int Goback);
                            if (Goback == 1)
                            {
                                Console.Clear();
                            }

                            break;
                            
                        case 4:
                            Console.Clear();
                            DataBase();
                            Console.Write("1번키를 누르면 돌아갑니다.");
                            int.TryParse(Console.ReadLine(), out int goback);
                            if (goback == 1)
                            {
                                Console.Clear();
                            }
                            break;
                        case 5:
                            Console.Clear();
                            Interest_Search();
                            Console.Clear();
                            break;
                        case 6:
                            Console.Clear();
                            repeat = false;
                            break;
                        case 7:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.Clear();
                            break;
                    }
                }
                
            }
            return InterestSubject;
        }

        //관심과목 목록
        public void Interest_Search()
        {
            
            while (repeat)
            {
                Console.Write("관 심 과 목 신 청\n\n\n");
                Console.Write("1. 개설 학과 전공으로 검색하여 관심 과목 신청\n\n");
                Console.Write("2. 학수 번호로 검색하여 관심 과목 신청\n\n");
                Console.Write("3. 교과목 명으로 검색하여 관심 과목 신청\n\n");
                Console.Write("4. 강의 대상 학년으로 검색하여 관심 과목 신청\n\n");
                Console.Write("5. 교수명으로 검색하여 관심 과목 신청\n\n");
                Console.Write("6.뒤로 가기;\n\n");
                Console.Write("7. 종료\n\n");
                Console.Write("원하는 번호를 고르시오.  : ");
                ConsoleKeyInfo Exam;
                Exam = Console.ReadKey();

                if (Exam.Key == ConsoleKey.Escape)
                {
                    Console.Clear();

                }


                else
                {
                    int.TryParse(Exam.KeyChar.ToString(), out int select);

                    switch (select)
                    {
                        case 1:
                            Console.Clear();
                            MajorSearch();
                            InterestInput();
                            InterestQuestion();
                            Console.Clear();
                            break;
                        case 2:
                            Console.Clear();
                            ClassNumberSearch();
                            InterestInput();
                            InterestQuestion();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ClassNameSearch();
                            InterestInput();
                            InterestQuestion();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            StudentGradeSearch();
                            InterestInput();
                            InterestQuestion();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            ProfessorNameSearch();
                            InterestInput();
                            InterestQuestion();
                            Console.Clear();
                            break;
                        case 6:
                            Console.Clear();
                            repeat = false;
                            break;
                        case 7:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        //NO로 관심과목 신청
        public void InterestInput()
        {
            bool repeat3 = true;
            while (repeat3)
            {
                Console.WriteLine("---------------------------------------------------------------------------------------------------------");
                Console.Write("관심과목 추가할 과목의 No.를 입력하세요 :");
                int.TryParse(Console.ReadLine(), out int Number);

                if (Number > 0 && Number < 180)
                {
                    NumberClass ClassNumber = new NumberClass();
                    ClassNumber.NO = Convert.ToString(ExcelData.GetValue(Number + 1, 1));
                    ClassNumber.Subject = Convert.ToString(ExcelData.GetValue(Number + 1, 2));
                    ClassNumber.ClassNumber = Convert.ToString(ExcelData.GetValue(Number + 1, 3));
                    ClassNumber.divide = Convert.ToString(ExcelData.GetValue(Number + 1, 4));
                    ClassNumber.ClassName = Convert.ToString(ExcelData.GetValue(Number + 1, 5));
                    ClassNumber.division = Convert.ToString(ExcelData.GetValue(Number + 1, 6));
                    ClassNumber.grade = Convert.ToString(ExcelData.GetValue(Number + 1, 7));
                    ClassNumber.credit = Convert.ToString(ExcelData.GetValue(Number + 1, 8));
                    ClassNumber.time = Convert.ToString(ExcelData.GetValue(Number + 1, 9));
                    ClassNumber.room = Convert.ToString(ExcelData.GetValue(Number + 1, 10));
                    ClassNumber.professor = Convert.ToString(ExcelData.GetValue(Number + 1, 11));
                    ClassNumber.language = Convert.ToString(ExcelData.GetValue(Number + 1, 12));
                    TotalGrade = TotalGrade + Convert.ToDouble(ClassNumber.credit);
                    int CheckName = 0;
                    int CheckNumber = 0;

                    if (InterestSubject.Count() == 0)
                    {
                        
                            InterestSubject.Add(ClassNumber);
                            Console.Write("관심과목 추가!!");
                            repeat3 = false;
             
                    }
                    else
                    {
                      if (TotalGrade > 24)
                        {
                            Console.Write("학점을 초과하여 추가할 수 없습니다.");
                            TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                            break;
                        }
                      else
                        {
                            for (int Overlap = 0; Overlap < InterestSubject.Count(); Overlap++)
                            {
                                if (ClassNumber.NO == InterestSubject[Overlap].NO )
                                {
                                    
                                    CheckName = CheckName +1; 
                                }
                                else if(ClassNumber.ClassNumber == InterestSubject[Overlap].ClassNumber)
                                {
                                    CheckNumber = CheckNumber + 1;
                                }
                            }
                            if (CheckName >= 1)
                            {
                                Console.Write("강의명이 중복됩니다");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            }
                            else if (CheckNumber >= 1)
                            {
                                Console.Write("학수번호가 중복됩니다");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            }
                            else
                            {
                                InterestSubject.Add(ClassNumber);
                                Console.Write("관심과목 추가!!");
                                repeat3 = false;
                                break;
                            }
                        }
                    }
                   
                }

                else
                {
                    Console.Write("잘못 입력하셨습니다. 다시 입력해주세요.");


                }
            }

        }


        //관심과목 추가 신청 질문
        public void InterestQuestion()
        {
            bool repeat2 = true;
            while (repeat2)
            {
                Console.Write("추가로 관심과목신청하시겠습니까?? \n 1. 추가신청 2. 관심과목신청");
                int.TryParse(Console.ReadLine(), out int back);
                if (back == 1)
                {
                    InterestInput();
                }
                else if (back == 2)
                {
                    repeat2 = false;
                }
                
            }
        }



        public void Title()
        {
            List<string> Title = new List<string>();
            for (int garo = 0; garo < 12; garo++)
            {
                Title.Add(Convert.ToString(ExcelData.GetValue(1, garo + 1)));
                Console.Write(Title[garo] + "   ");
                if (garo == 11)
                {
                    Console.Write("\n");
                }


            }


        }


        //학과명으로 수강신청 검색
        public void MajorSearch()
        {


            Console.Write("학과명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 2)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }



        }


        //학수번호로 수강신청 검색
        public void ClassNumberSearch()
        {


            Console.Write("학수번호를 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 3)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }

        //교과목명으로 검색하여 수강신청
        public void ClassNameSearch()
        {



            Console.Write("교과목명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();

            Title();
            //맨위에 목록들 그리는거
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 5)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }


        //대상학년으로 검색하여 수강신청
        public void StudentGradeSearch()
        {



            Console.Write("대상학년을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 7)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }

        //교수명으로 검색하여 수강신청
        public void ProfessorNameSearch()
        {


            Console.Write("교수명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 11)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }


        //강의 목록 전체를 출력하는 함수
        public void DataBase()
        {

            //엑셀 데이터 값을 전부 출력하는 함수
            for (int sero = 1; sero < 181; sero++)
            {
                for (int garo = 1; garo < 13; garo++)
                {
                    //string TotalData = Convert.ToString((cellRange.Cells[sero, garo] as Excel.Range).Value2); 
                    string TotalData = Convert.ToString(ExcelData.GetValue(sero, garo));

                    Console.Write(TotalData + "  ");


                    if (garo == 12)
                    {
                        Console.Write("\n");
                    }
                }
            }


        }


        //수강신청 목록 조회 메소드
        public void InterestCheck()
        {
            int Num = 1;
            for (int total = 0; total < InterestSubject.Count(); total++)
            {

                Console.Write(Num + "  ");
                InterestSubject[total].Output();
                Num++;
            }
        }


        //수강신청 삭제 메소드
        public void InterestRemove()
        {
            bool repeat6 = true;
            while (repeat6)
            {


                bool repeat5 = true;
                while (repeat5)
                {
                    Console.Write("어떤 강의를 지우시겠습니까?? No를 입력해주세요.");
                    int.TryParse(Console.ReadLine(), out int RemoveCheck);
                   
                            InterestSubject.RemoveAt(RemoveCheck-1);
                            Console.Clear();
                            Title();
                            InterestCheck();
                            repeat5 = false;
                        
                    
                }
                Console.Write("강의를 더 삭제하시겠습니까?     1.예 2. 아니오\n");
                int.TryParse(Console.ReadLine(), out int RemoveSelect);
                if (RemoveSelect == 1)
                {
                    repeat6 = true;
                }
                else if (RemoveSelect == 2)
                {
                    repeat6 = false;
                    Console.Clear();
                }

            }

        }







    }
}
