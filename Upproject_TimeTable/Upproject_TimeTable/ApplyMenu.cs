﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Upproject_TimeTable
{
    class ApplyMenu
    {
        public Array ExcelData = new Array[300];
        public List<NumberClass> ApplySubject = new List<NumberClass>();
        public List<NumberClass> InterestSubject = new List<NumberClass>();
        bool repeat = true;
        double TotalGrade = 0;
        Dictionary<string, int> DictionaryTime = new Dictionary<string, int>()
        {
            {"08:00",1 },{"08:30",2 },{"09:00",3 },{"09:30",4 },{"10:00",5 },{"10:30",6 },{"11:00",7 },{"11:30",8 },{"12:00",9 }
            ,{"12:30",10 },{"13:00",11 },{"13:30",12 },{"14:00",13 },{"14:30",14 },{"15:00",15 },{"15:30",16 },{"16:00",17 },{"16:30",18 }
            ,{"17:00",19 },{"17:30",20 },{"18:00",21 },{"18:30",22 },{"19:00",23 },{"19:30",24 },{"20:00",25 }
        };

        Dictionary<string, int> Dictionaryweek = new Dictionary<string, int>()
        {
            {"월",31 },{"화",32 },{"수",33 },{"목",34 },{"금",35 }
        };

        
        //수강신청 메뉴
        public List<NumberClass> Apply(List<NumberClass> ApplySubject, Array ExcelData,List<NumberClass> InterestSubject)
        {
            while (repeat)
            {
                this.InterestSubject = InterestSubject;
                this.ApplySubject = ApplySubject;
                this.ExcelData = ExcelData;
                Console.Write("       수     강     신      청\n\n\n\n\n");
                Console.Write("1. 수강강의 추가\n\n");
                Console.Write("2. 수강강의 삭제\n\n");
                Console.Write("3. 수강강의 조회\n\n");
                Console.Write("4. 전체강의 목록\n\n");
                Console.Write("5.  강 의 검 색\n\n");
                Console.Write("6. 처음메뉴로 돌아가기\n\n");
                Console.Write("7.  종       료\n\n");
                Console.Write(" 원하는 번호를 고르시오. :");
                ConsoleKeyInfo Exam;
                Exam = Console.ReadKey();

                if (Exam.Key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    break;
                }


                else
                {
                    int.TryParse(Exam.KeyChar.ToString(), out int select);

                    switch (select)
                    {
                        case 1:
                            Console.Clear();
                            bool repeat4 = true;
                            while (repeat4)
                            {
                                ApplyInput();
                                Console.Write("다시 신청하시겠습니까?     1.Yes 2.No\n");
                                int.TryParse(Console.ReadLine(), out int repeatcheck);
                                if (repeatcheck == 1)
                                {
                                    repeat4 = true;
                                }
                                else if (repeatcheck == 2)
                                {
                                    repeat4 = false;
                                    Console.Clear();
                                }


                            }

                            break;
                        case 2:
                            Console.Clear();
                            Title();
                            ApplyCheck();
                            ApplyRemove();
                            break;
                        case 3:
                            Console.Clear();//이거 돌아가는 함수는 없나???
                            Title();
                            //for(int i =0; i<Applysubject.Length;i++){Applysubject[i].Output();}
                            ApplyCheck();
                            Console.Write("\n 총학점 :  ");
                            Console.Write(TotalGrade + "\n");
                            Console.Write("1번키를 누르면 돌아갑니다.");
                            int.TryParse(Console.ReadLine(), out int Goback);
                            if (Goback == 1)
                            {
                                Console.Clear();
                            }
                            
                            break;
                        case 4:
                            Console.Clear();
                            DataBase();
                            Console.Write("1번키를 누르면 돌아갑니다.");
                            int.TryParse(Console.ReadLine(), out int goback);
                            if (goback == 1)
                            {
                                Console.Clear();
                            }
                            break;
                        case 5:
                            Console.Clear();
                            SubjectSearch();
                            break;
                        case 6:
                            Console.Clear();
                            repeat = false;
                            break;
                        case 7:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.Clear();
                            break;
                    }
                }

                
            }
            return ApplySubject;
        }



        //수강강의 검색 메뉴

        public void SubjectSearch()
        {
            bool repeat = true;
            while (repeat)
            {
                Console.Write("  수  강  과  목  검  색\n\n\n\n");
                Console.Write("1. 개설 학과 전공으로 검색하여 수강 신청\n\n");
                Console.Write("2. 학수 번호로 검색하여 수강 신청\n\n");
                Console.Write("3. 교과목 명으로 검색하여 수강 신청\n\n");
                Console.Write("4. 강의 대상 학년으로 검색하여 수강 신청\n\n");
                Console.Write("5. 교수명으로 검색하여 수강 신청\n\n");
                Console.Write("6. 관심 과목으로 검색하여 수강 신청\n\n");
                Console.Write("7. 뒤로 가기\n\n");  //(없어도 되는 기능 아닌가??)
                Console.Write("8. 종료\n\n");
                Console.Write("원하는 번호를 고르시오.  : ");
                ConsoleKeyInfo Exam;
                Exam = Console.ReadKey();

                if (Exam.Key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    break;
                }


                else
                {
                    int.TryParse(Exam.KeyChar.ToString(), out int select);

                    switch (select)
                    {
                        case 1:
                            Console.Clear();
                            MajorSearch();
                            ApplyInput();
                            ApplyQuestion();
                            Console.Clear();
                            break;
                        case 2:
                            Console.Clear();
                            ClassNumberSearch();
                            ApplyInput();
                            ApplyQuestion();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ClassNameSearch();
                            ApplyInput();
                            ApplyQuestion();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            StudentGradeSearch();
                            ApplyInput();
                            ApplyQuestion();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            ProfessorNameSearch();
                            ApplyInput();
                            ApplyQuestion();
                            Console.Clear();
                            break;
                        case 6:
                            Console.Clear();
                            Title();
                            InterestApply();
                            InterestInput();
                            ApplyQuestion();
                            Console.Clear();
                            //관심과목
                            break;
                        case 7:
                            Console.Clear();
                            repeat = false;
                            break;
                        case 8:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.Clear();
                            break;
                    }
                    
                }
            }
        }
        
        //수강신청을 추가로 할 건지 물어보는 부분
        public void ApplyQuestion()
        {
            bool repeat2 = true;
            while (repeat2)
            {
                Console.Write("추가로 수강신청하시겠습니까?? \n 1. 추가신청 2. 수강신청검색 ");
                int.TryParse(Console.ReadLine(), out int back);
                if (back == 1)
                {
                    ApplyInput();
                }
                else if (back == 2)
                {
                    
                    repeat2 = false;
                }
               
            }
        }

        //신청할 강의 NO로 입력받기(학점제한도 성공)

        public void ApplyInput()
        {
            bool repeat3 = true;
            while (repeat3)
            {
                Console.WriteLine("---------------------------------------------------------------------------------------------------------");
                Console.Write("수강신청할 과목의 No.를 입력하세요 :");
                int.TryParse(Console.ReadLine(), out int Number);

                if (Number > 0 && Number < 180)
                {
                    NumberClass ClassNumber = new NumberClass();
                    ClassNumber.NO = Convert.ToString(ExcelData.GetValue(Number + 1, 1));
                    ClassNumber.Subject = Convert.ToString(ExcelData.GetValue(Number + 1, 2));
                    ClassNumber.ClassNumber = Convert.ToString(ExcelData.GetValue(Number + 1, 3));
                    ClassNumber.divide = Convert.ToString(ExcelData.GetValue(Number + 1, 4));
                    ClassNumber.ClassName = Convert.ToString(ExcelData.GetValue(Number + 1, 5));
                    ClassNumber.division = Convert.ToString(ExcelData.GetValue(Number + 1, 6));
                    ClassNumber.grade = Convert.ToString(ExcelData.GetValue(Number + 1, 7));
                    ClassNumber.credit = Convert.ToString(ExcelData.GetValue(Number + 1, 8));
                    ClassNumber.time = Convert.ToString(ExcelData.GetValue(Number + 1, 9));
                    ClassNumber.room = Convert.ToString(ExcelData.GetValue(Number + 1, 10));
                    ClassNumber.professor = Convert.ToString(ExcelData.GetValue(Number + 1, 11));
                    ClassNumber.language = Convert.ToString(ExcelData.GetValue(Number + 1, 12));
                    TotalGrade = TotalGrade + Convert.ToDouble(ClassNumber.credit);
                    int CheckName = 0;
                    int CheckNumber = 0;
                    int CheckTime = 0;

                    if (ApplySubject.Count() == 0)
                    {
                        
                            ApplySubject.Add(ClassNumber);
                            Console.Write("수강신청 성공!!");
                            repeat3 = false;
                            break;
                    }
                    else
                    {
                        if (TotalGrade > 24)
                        {
                            Console.Write("학점을 초과하여 수강신청할 수 없습니다.");
                            TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                            break;
                        }
                        else
                        {
                            for (int Overlap = 0; Overlap < ApplySubject.Count(); Overlap++)
                            {
                                if (ClassNumber.NO == ApplySubject[Overlap].NO )
                                {
                                    CheckName = CheckName + 1;
                                }
                                else if(ClassNumber.ClassNumber == ApplySubject[Overlap].ClassNumber)
                                {
                                    CheckNumber = CheckNumber + 1;
                                }
                            }
                            for(int ListNumber =0; ListNumber < ApplySubject.Count(); ListNumber++)
                            {
                                string[] DayTime = ApplySubject[ListNumber].time.Split(Convert.ToChar((" ")));
                                string[] DayTimeComp = ClassNumber.time.Split(Convert.ToChar((" ")));
                                if (DayTimeComp.Length == 2)
                                {
                                    string DayComp = DayTimeComp[0];
                                    string TimeComp = DayTimeComp[1].Substring(0, 5);
                                    string TimebackComp = DayTimeComp[1].Substring(6, 5);

                                    if (DayTime.Length == 2)
                                    {
                                        string Day = DayTime[0];
                                        string Time = DayTime[1].Substring(0, 5);
                                        string Timeback = DayTime[1].Substring(6, 5);
                                        for(int CompFirst = DictionaryTime[Time]; CompFirst < DictionaryTime[Timeback]; CompFirst++)
                                        {
                                            for(int CompSecond = DictionaryTime[TimeComp]; CompSecond < DictionaryTime[TimebackComp]; CompSecond++)
                                            {
                                                if (DayComp == Day && CompSecond == CompFirst)
                                                {
                                                    CheckTime = CheckTime + 1;
                                                }
                                            }
                                        }
                                    }

                                    if(DayTime.Length == 3)
                                    {
                                        string Day = DayTime[0];
                                        string Day2 = DayTime[1];
                                        string Time = DayTime[2].Substring(0, 5);
                                        string Timeback = DayTime[2].Substring(6, 5);
                                        for(int CompFirst = DictionaryTime[Time];CompFirst<DictionaryTime[Timeback];CompFirst++)
                                        {
                                            for (int CompSecond = DictionaryTime[TimeComp]; CompSecond < DictionaryTime[TimebackComp]; CompSecond++)
                                            {
                                                if ((DayComp == Day && CompSecond == CompFirst)||(DayComp==Day2&&CompSecond==CompFirst))
                                                {
                                                    CheckTime = CheckTime + 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (CheckName >= 1)
                            {
                                Console.Write("강의명이 중복됩니다.");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            } 
                            else if (CheckNumber >= 1)
                            {
                                Console.Write("학수번호가 중복됩니다");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            }
                            else
                            {
                                ApplySubject.Add(ClassNumber);
                                Console.Write("수강신청 성공!!");
                                repeat3 = false;
                                break;
                            }
                        }
                       
                    }
                }

                else
                {
                    Console.Write("잘못 입력하셨습니다. 다시 입력해주세요.");


                }
            }

        }


        public void InterestInput()
        {
            bool repeat3 = true;
            while (repeat3)
            {
                Console.WriteLine("---------------------------------------------------------------------------------------------------------");
                Console.Write("수강신청할 과목의 No.를 입력하세요 :");
                int.TryParse(Console.ReadLine(), out int Number);

                if (Number > 0 && Number < 180)
                {
                    NumberClass ClassNumber = new NumberClass();
                    ClassNumber.NO = Convert.ToString(InterestSubject[Number-1].NO);
                    ClassNumber.Subject = Convert.ToString(InterestSubject[Number -1].Subject);
                    ClassNumber.ClassNumber = Convert.ToString(InterestSubject[Number -1].ClassNumber);
                    ClassNumber.divide = Convert.ToString(InterestSubject[Number -1].divide);
                    ClassNumber.ClassName = Convert.ToString(InterestSubject[Number -1].ClassName);
                    ClassNumber.division = Convert.ToString(InterestSubject[Number -1].division);
                    ClassNumber.grade = Convert.ToString(InterestSubject[Number -1].grade);
                    ClassNumber.credit = Convert.ToString(InterestSubject[Number -1].credit);
                    ClassNumber.time = Convert.ToString(InterestSubject[Number -1].time);
                    ClassNumber.room = Convert.ToString(InterestSubject[Number -1].room);
                    ClassNumber.professor = Convert.ToString(InterestSubject[Number -1].professor);
                    ClassNumber.language = Convert.ToString(InterestSubject[Number -1].language);
                    TotalGrade = TotalGrade + Convert.ToDouble(ClassNumber.credit);
                    int CheckName = 0;
                    int CheckNumber = 0;

                    if (InterestSubject.Count() == 0)
                    {

                        InterestSubject.Add(ClassNumber);
                        Console.Write("관심과목 추가!!");
                        repeat3 = false;

                    }
                    else
                    {
                        if (TotalGrade > 24)
                        {
                            Console.Write("학점을 초과하여 추가할 수 없습니다.");
                            TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                            break;
                        }
                        else
                        {
                            for (int Overlap = 0; Overlap < InterestSubject.Count(); Overlap++)
                            {
                                if (ClassNumber.NO == InterestSubject[Overlap].NO)
                                {

                                    CheckName = CheckName + 1;
                                }
                                else if (ClassNumber.ClassNumber == InterestSubject[Overlap].ClassNumber)
                                {
                                    CheckNumber = CheckNumber + 1;
                                }
                            }
                            if (CheckName >= 1)
                            {
                                Console.Write("강의명이 중복됩니다");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            }
                            else if (CheckNumber >= 1)
                            {
                                Console.Write("학수번호가 중복됩니다");
                                TotalGrade = TotalGrade - Convert.ToDouble(ClassNumber.credit);
                                break;
                            }
                            else
                            {
                                InterestSubject.Add(ClassNumber);
                                Console.Write("관심과목 추가!!");
                                repeat3 = false;
                                break;
                            }
                        }
                    }

                }

                else
                {
                    Console.Write("잘못 입력하셨습니다. 다시 입력해주세요.");


                }
            }

        }


        //맨위의 목록을 불러오는 함수
        public void Title()
        {
            List<string> Title = new List<string>();
            for (int garo = 0; garo < 12; garo++)
            {
                Title.Add(Convert.ToString(ExcelData.GetValue(1, garo + 1)));
                Console.Write(Title[garo] + "   ");
                if (garo == 11)
                {
                    Console.Write("\n");
                }


            }


        }

        



        //학과명으로 수강신청 검색
        public void MajorSearch()
        {


            Console.Write("학과명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 2)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }



        }



        //학수번호로 수강신청 검색
        public void ClassNumberSearch()
        {


            Console.Write("학수번호를 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 3)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }



        //교과목명으로 검색하여 수강신청
        public void ClassNameSearch()
        {



            Console.Write("교과목명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();

            Title();
            //맨위에 목록들 그리는거
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 5)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }

        //대상학년으로 검색하여 수강신청
        public void StudentGradeSearch()
        {



            Console.Write("대상학년을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 7)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }

        //교수명으로 검색하여 수강신청
        public void ProfessorNameSearch()
        {


            Console.Write("교수명을 입력해주세요 : ");
            string Condition = Console.ReadLine();
            Console.Clear();
            Title();
            for (int Repeat = 1; Repeat < 178; Repeat++)
            {
                if (Convert.ToString(ExcelData.GetValue(Repeat, 11)).Contains(Condition))
                {

                    for (int garo = 1; garo < 12; garo++)
                    {
                        Console.Write(Convert.ToString(ExcelData.GetValue(Repeat, garo) + "  "));

                        if (garo == 11)
                        {
                            Console.Write("\n");
                        }


                    }
                }
            }

        }

        //강의 목록 전체를 출력하는 함수
        public void DataBase()
        {

            //엑셀 데이터 값을 전부 출력하는 함수
            for (int sero = 1; sero < 181; sero++)
            {
                for (int garo = 1; garo < 13; garo++)
                {
                    //string TotalData = Convert.ToString((cellRange.Cells[sero, garo] as Excel.Range).Value2); 
                    string TotalData = Convert.ToString(ExcelData.GetValue(sero, garo));

                    switch (garo)
                    {
                        case 1:
                            Console.Write(TotalData.PadRight(3)+"  ");
                            break;
                        case 2:
                            Console.Write(TotalData.PadRight(8) + "  ");
                            break;
                        case 3:
                            Console.Write(TotalData.PadRight(6) + "  ");
                            break;
                        case 4:
                            Console.Write(TotalData.PadRight(3) + "  ");
                            break;
                        case 5:
                            Console.Write(TotalData.PadRight(10) + "  ");
                            break;
                        case 6:
                            Console.Write(TotalData.PadRight(4) + "  ");
                            break;
                        case 7:
                            Console.Write(TotalData.PadRight(1) + "  ");
                            break;
                        case 8:
                            Console.Write(TotalData.PadRight(3) + "  ");
                            break;
                        case 9:
                            Console.Write(TotalData.PadRight(10) + "  ");
                            break;
                        case 10:
                            Console.Write(TotalData.PadRight(7) + "  ");
                            break;
                        case 11:
                            Console.Write(TotalData.PadRight(10) + "  ");
                            break;
                        case 12:
                            Console.Write(TotalData.PadRight(3) + "  ");
                            Console.Write("\n");
                            break;

                    }
                   
                }
            }


        }

        //수강신청 목록 조회 메소드
        public void ApplyCheck()
        {
            int Num = 1;
            for (int total = 0; total < ApplySubject.Count(); total++)
            {
                
                Console.Write(Num + " ");
                ApplySubject[total].Output2();
                Num++;
            }
        }

       

        //수강신청 삭제 메소드
        public void  ApplyRemove()
        {
            bool repeat6 = true;
            while (repeat6)
            {


                bool repeat5 = true;
                while (repeat5)
                {
                    Console.Write("어떤 강의를 지우시겠습니까?? No를 입력해주세요. 0 = 돌아가기");
                    int.TryParse(Console.ReadLine(), out int RemoveCheck);

                    if (RemoveCheck == 0)
                    {
                        Console.Clear();

                        break;
                    }

                    TotalGrade = TotalGrade - Convert.ToDouble(ApplySubject[RemoveCheck-1].credit);
                            ApplySubject.RemoveAt(RemoveCheck-1);
                            Console.Clear();
                            Title();
                            ApplyCheck();
                            repeat5 = false;
                    
                    
                }
                Console.Write("강의를 더 삭제하시겠습니까?     1.예 2. 아니오\n");
                int.TryParse(Console.ReadLine(), out int RemoveSelect);
                if (RemoveSelect == 1)
                {
                    repeat6 = true;
                }
                else if (RemoveSelect == 2)
                {
                    repeat6 = false;
                    Console.Clear();
                }

            }

        }

        //관심과목 목록 불러오는 함수
        public void InterestApply()
        {
            int Num = 1;
            for (int total = 0; total < InterestSubject.Count(); total++)
            {

                Console.Write(Num + "  ");
                InterestSubject[total].Output2();
                Num++;
            }
        }




    }
}

    
