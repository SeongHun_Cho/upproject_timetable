﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upproject_TimeTable
{
    class MyTimeTable
    {
        public Array ExcelData = new Array[300];
        public List<NumberClass> ApplySubject = new List<NumberClass>();
        string[,] TimeTableName = new string[5, 25];
        string[,] TimeTableRoom = new string[5, 25];
        Dictionary<string, int> TimeTableDictionary = new Dictionary<string, int>()
        {
            {"08:00",1 },{"08:30",2 },{"09:00",3 },{"09:30",4 },{"10:00",5 },{"10:30",6 },{"11:00",7 },{"11:30",8 },{"12:00",9 }
            ,{"12:30",10 },{"13:00",11 },{"13:30",12 },{"14:00",13 },{"14:30",14 },{"15:00",15 },{"15:30",16 },{"16:00",17 },{"16:30",18 }
            ,{"17:00",19 },{"17:30",20 },{"18:00",21 },{"18:30",22 },{"19:00",23 },{"19:30",24 },{"20:00",25 }
        };


        //나의시간표 틀
        public void TimeTable(List<NumberClass> ApplySubject, Array ExcelData)
        {
            for(int start = 0; start < 5; start++)
            {
                for(int end = 1; end < 25; end++)
                {
                    TimeTableName[start, end] = " ";
                    TimeTableRoom[start, end] = " ";
                }
            }
            this.ApplySubject = ApplySubject;
            this.ExcelData = ExcelData;
            int Sero = 0;
            int Garo = 0;
            int Sero2 = 0;
            int Garo2 = 0;
            int Garo3 = 0;
           for(int reload = 0; reload < ApplySubject.Count; reload++)
            {
                string[] DayTime = ApplySubject[reload].time.Split(Convert.ToChar(" "));

                if (DayTime.Length == 2)
                {
                    string Day = DayTime[0];
                    string Time = DayTime[1].Substring(0,5);
                    string Timeback = DayTime[1].Substring(6, 5);

                    switch(Day){
                        case "월": Garo = 0; break;
                        case "화": Garo = 1; break;
                        case "수": Garo = 2; break;
                        case "목": Garo = 3; break;
                        case "금": Garo = 4; break;
                    }
                  
                    for (Sero = TimeTableDictionary[Time]; Sero < TimeTableDictionary[Timeback]; Sero++)
                    {


                        TimeTableName[Garo, Sero] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo, Sero] = ApplySubject[reload].room;
                    }
                }

                else if (DayTime.Length == 3)
                {
                    string Day = DayTime[0];
                    string Day2 = DayTime[1];
                    string Time = DayTime[2].Substring(0, 5);
                    string Timeback = DayTime[2].Substring(6, 5);

                    switch (Day)
                    {
                        case "월": Garo = 0; break;
                        case "화": Garo = 1; break;
                        case "수": Garo = 2; break;
                        case "목": Garo = 3; break;
                        case "금": Garo = 4; break;
                    }

                    switch (Day2)
                    {
                        case "월": Garo2 = 0; break;
                        case "화": Garo2 = 1; break;
                        case "수": Garo2 = 2; break;
                        case "목": Garo2 = 3; break;
                        case "금": Garo2 = 4; break;
                    }

                    for (Sero = TimeTableDictionary[Time]; Sero < TimeTableDictionary[Timeback]; Sero++)
                    {


                        TimeTableName[Garo, Sero] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo, Sero] = ApplySubject[reload].room;
                        TimeTableName[Garo2, Sero] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo2, Sero] = ApplySubject[reload].room;
                    }
                }
               

                else if (DayTime.Length == 5)
                {
                    string Day = DayTime[0];
                    string Day2 = DayTime[1];
                    string Time = DayTime[2].Substring(0, 5);
                    string Timeback = DayTime[2].Substring(6, 5);
                    string Day3 = DayTime[3];
                    string Time2 = DayTime[4].Substring(0, 5);
                    string Timeback2 = DayTime[4].Substring(6, 5);

                    switch (Day)
                    {
                        case "월": Garo = 0; break;
                        case "화": Garo = 1; break;
                        case "수": Garo = 2; break;
                        case "목": Garo = 3; break;
                        case "금": Garo = 4; break;
                    }

                    switch (Day2)
                    {
                        case "월": Garo2 = 0; break;
                        case "화": Garo2 = 1; break;
                        case "수": Garo2 = 2; break;
                        case "목": Garo2 = 3; break;
                        case "금": Garo2 = 4; break;
                    }

                    
                    switch (Day3)
                    {
                        case "월": Garo3 = 0; break;
                        case "화": Garo3 = 1; break;
                        case "수": Garo3 = 2; break;
                        case "목": Garo3 = 3; break;
                        case "금": Garo3 = 4; break;
                    }
                    
                    for (Sero = TimeTableDictionary[Time]; Sero < TimeTableDictionary[Timeback]; Sero++)
                    {
                        TimeTableName[Garo, Sero] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo, Sero] = ApplySubject[reload].room;
                        TimeTableName[Garo2, Sero] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo2, Sero] = ApplySubject[reload].room;
                    }
                    for (Sero2 = TimeTableDictionary[Time2]; Sero2 < TimeTableDictionary[Timeback2]; Sero2++){
                        TimeTableName[Garo3, Sero2] = ApplySubject[reload].ClassName;
                        TimeTableRoom[Garo3, Sero2] = ApplySubject[reload].room;
                    }
                }


            }
            
        }


        public void TimeTableBackGround()
        {/*
            Console.WriteLine("             월                   화                     수                      목                       금           ");
            Console.WriteLine("08:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableRoom[0, 1] + "     |     " + TimeTableName[1, 1] + "     |     " + TimeTableName[2, 1] + "     |     " + TimeTableName[3, 1] + "     |     " + TimeTableName[4, 1] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 1] + "     |     " + TimeTableRoom[1, 1] + "     |     " + TimeTableRoom[2, 1] + "     |     " + TimeTableRoom[3, 1] + "     |     " + TimeTableRoom[4, 1] + "     |     ");
            Console.WriteLine("08:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 2].PadRight(7) + "     |     " + TimeTableName[1, 2].PadRight(7) + "     |     " + TimeTableName[2, 2].PadRight(7) + "     |     " + TimeTableName[3, 2].PadRight(7) + "     |     " + TimeTableName[4, 2] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 2].PadRight(7) + "     |     " + TimeTableRoom[1, 2].PadRight(7) + "     |     " + TimeTableRoom[2, 2].PadRight(7) + "     |     " + TimeTableRoom[3, 2].PadRight(7) + "     |     " + TimeTableRoom[4, 2] + "     |     ");
            Console.WriteLine("09:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 3].PadRight(7) + "     |     " + TimeTableName[1, 3].PadRight(7) + "     |     " + TimeTableName[2, 3].PadRight(7) + "     |     " + TimeTableName[3, 3].PadRight(7) + "     |     " + TimeTableName[4, 3] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 3].PadRight(7) + "     |     " + TimeTableRoom[1, 3].PadRight(7) + "     |     " + TimeTableRoom[2, 3].PadRight(7) + "     |     " + TimeTableRoom[3, 3].PadRight(7) + "     |     " + TimeTableRoom[4, 3] + "     |     ");
            Console.WriteLine("09:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 4].PadRight(7) + "     |     " + TimeTableName[1, 4].PadRight(7) + "     |     " + TimeTableName[2, 4].PadRight(7) + "     |     " + TimeTableName[3, 4].PadRight(7) + "     |     " + TimeTableName[4, 4] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 4].PadRight(7) + "     |     " + TimeTableRoom[1, 4].PadRight(7) + "     |     " + TimeTableRoom[2, 4].PadRight(7) + "     |     " + TimeTableRoom[3, 4].PadRight(7) + "     |     " + TimeTableRoom[4, 4] + "     |     ");
            Console.WriteLine("10:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 5] + "     |     " + TimeTableName[1, 5] + "     |     " + TimeTableName[2, 5] + "     |     " + TimeTableName[3, 5] + "     |     " + TimeTableName[4, 5] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 5] + "     |     " + TimeTableRoom[1, 5] + "     |     " + TimeTableRoom[2, 5] + "     |     " + TimeTableRoom[3, 5] + "     |     " + TimeTableRoom[4, 5] + "     |     ");
            Console.WriteLine("10:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 6] + "     |     " + TimeTableName[1, 6] + "     |     " + TimeTableName[2, 6] + "     |     " + TimeTableName[3, 6] + "     |     " + TimeTableName[4, 6] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 6] + "     |     " + TimeTableRoom[1, 6] + "     |     " + TimeTableRoom[2, 6] + "     |     " + TimeTableRoom[3, 6] + "     |     " + TimeTableRoom[4, 6] + "     |     ");
            Console.WriteLine("11:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 7] + "     |     " + TimeTableName[1, 7] + "     |     " + TimeTableName[2, 7] + "     |     " + TimeTableName[3, 7] + "     |     " + TimeTableName[4, 7] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 7] + "     |     " + TimeTableRoom[1, 7] + "     |     " + TimeTableRoom[2, 7] + "     |     " + TimeTableRoom[3, 7] + "     |     " + TimeTableRoom[4, 7] + "     |     ");
            Console.WriteLine("11:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 8] + "     |     " + TimeTableName[1, 8] + "     |     " + TimeTableName[2, 8] + "     |     " + TimeTableName[3, 8] + "     |     " + TimeTableName[4, 8] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 8] + "     |     " + TimeTableRoom[1, 8] + "     |     " + TimeTableRoom[2, 8] + "     |     " + TimeTableRoom[3, 8] + "     |     " + TimeTableRoom[4, 8] + "     |     ");
            Console.WriteLine("12:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 9] + "     |     " + TimeTableName[1, 9] + "     |     " + TimeTableName[2, 9] + "     |     " + TimeTableName[3, 9] + "     |     " + TimeTableName[4, 9] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 9] + "     |     " + TimeTableRoom[1, 9] + "     |     " + TimeTableRoom[2, 9] + "     |     " + TimeTableRoom[3, 9] + "     |     " + TimeTableRoom[4, 9] + "     |     ");
            Console.WriteLine("12:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 10] + "     |     " + TimeTableName[1, 10] + "     |     " + TimeTableName[2, 10] + "     |     " + TimeTableName[3, 10] + "     |     " + TimeTableName[4, 10] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 10] + "     |     " + TimeTableRoom[1, 10] + "     |     " + TimeTableRoom[2, 10] + "     |     " + TimeTableRoom[3, 10] + "     |     " + TimeTableRoom[4, 10] + "     |     ");
            Console.WriteLine("13:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 11] + "     |     " + TimeTableName[1, 11] + "     |     " + TimeTableName[2, 11] + "     |     " + TimeTableName[3, 11] + "     |     " + TimeTableName[4, 11] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 11] + "     |     " + TimeTableRoom[1, 11] + "     |     " + TimeTableRoom[2, 11] + "     |     " + TimeTableRoom[3, 11] + "     |     " + TimeTableRoom[4, 11] + "     |     ");
            Console.WriteLine("13:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 12] + "     |     " + TimeTableName[1, 12] + "     |     " + TimeTableName[2, 12] + "     |     " + TimeTableName[3, 12] + "     |     " + TimeTableName[4, 12] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 12] + "     |     " + TimeTableRoom[1, 12] + "     |     " + TimeTableRoom[2, 12] + "     |     " + TimeTableRoom[3, 12] + "     |     " + TimeTableRoom[4, 12] + "     |     ");
            Console.WriteLine("14:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 13] + "     |     " + TimeTableName[1, 13] + "     |     " + TimeTableName[2, 13] + "     |     " + TimeTableName[3, 13] + "     |     " + TimeTableName[4, 13] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 13] + "     |     " + TimeTableRoom[1, 13] + "     |     " + TimeTableRoom[2, 13] + "     |     " + TimeTableRoom[3, 13] + "     |     " + TimeTableRoom[4, 13] + "     |     ");
            Console.WriteLine("14:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 14] + "     |     " + TimeTableName[1, 14] + "     |     " + TimeTableName[2, 14] + "     |     " + TimeTableName[3, 14] + "     |     " + TimeTableName[4, 14] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 14] + "     |     " + TimeTableRoom[1, 14] + "     |     " + TimeTableRoom[2, 14] + "     |     " + TimeTableRoom[3, 14] + "     |     " + TimeTableRoom[4, 14] + "     |     ");
            Console.WriteLine("15:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 15] + "     |     " + TimeTableName[1, 15] + "     |     " + TimeTableName[2, 15] + "     |     " + TimeTableName[3, 15] + "     |     " + TimeTableName[4, 15] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 15] + "     |     " + TimeTableRoom[1, 15] + "     |     " + TimeTableRoom[2, 15] + "     |     " + TimeTableRoom[3, 15] + "     |     " + TimeTableRoom[4, 15] + "     |     ");
            Console.WriteLine("15:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 16] + "     |     " + TimeTableName[1, 16] + "     |     " + TimeTableName[2, 16] + "     |     " + TimeTableName[3, 16] + "     |     " + TimeTableName[4, 16] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 16] + "     |     " + TimeTableRoom[1, 16] + "     |     " + TimeTableRoom[2, 16] + "     |     " + TimeTableRoom[3, 16] + "     |     " + TimeTableRoom[4, 16] + "     |     ");
            Console.WriteLine("16:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 17] + "     |     " + TimeTableName[1, 17] + "     |     " + TimeTableName[2, 17] + "     |     " + TimeTableName[3, 17] + "     |     " + TimeTableName[4, 17] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 17] + "     |     " + TimeTableRoom[1, 17] + "     |     " + TimeTableRoom[2, 17] + "     |     " + TimeTableRoom[3, 17] + "     |     " + TimeTableRoom[4, 17] + "     |     ");
            Console.WriteLine("16:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 18] + "     |     " + TimeTableName[1, 18] + "     |     " + TimeTableName[2, 18] + "     |     " + TimeTableName[3, 18] + "     |     " + TimeTableName[4, 18] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 18] + "     |     " + TimeTableRoom[1, 18] + "     |     " + TimeTableRoom[2, 18] + "     |     " + TimeTableRoom[3, 18] + "     |     " + TimeTableRoom[4, 18] + "     |     ");
            Console.WriteLine("17:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 19] + "     |     " + TimeTableName[1, 19] + "     |     " + TimeTableName[2, 19] + "     |     " + TimeTableName[3, 19] + "     |     " + TimeTableName[4, 19] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 19] + "     |     " + TimeTableRoom[1, 19] + "     |     " + TimeTableRoom[2, 19] + "     |     " + TimeTableRoom[3, 19] + "     |     " + TimeTableRoom[4, 19] + "     |     ");
            Console.WriteLine("17:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 20] + "     |     " + TimeTableName[1, 20] + "     |     " + TimeTableName[2, 20] + "     |     " + TimeTableName[3, 20] + "     |     " + TimeTableName[4, 20] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 20] + "     |     " + TimeTableRoom[1, 20] + "     |     " + TimeTableRoom[2, 20] + "     |     " + TimeTableRoom[3, 20] + "     |     " + TimeTableRoom[4, 20] + "     |     ");
            Console.WriteLine("18:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 21] + "     |     " + TimeTableName[1, 21] + "     |     " + TimeTableName[2, 21] + "     |     " + TimeTableName[3, 21] + "     |     " + TimeTableName[4, 21] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 21] + "     |     " + TimeTableRoom[1, 21] + "     |     " + TimeTableRoom[2, 21] + "     |     " + TimeTableRoom[3, 21] + "     |     " + TimeTableRoom[4, 21] + "     |     ");
            Console.WriteLine("18:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 22] + "     |     " + TimeTableName[1, 22] + "     |     " + TimeTableName[2, 22] + "     |     " + TimeTableName[3, 22] + "     |     " + TimeTableName[4, 22] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 22] + "     |     " + TimeTableRoom[1, 22] + "     |     " + TimeTableRoom[2, 22] + "     |     " + TimeTableRoom[3, 22] + "     |     " + TimeTableRoom[4, 22] + "     |     ");
            Console.WriteLine("19:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 23] + "     |     " + TimeTableName[1, 23] + "     |     " + TimeTableName[2, 23] + "     |     " + TimeTableName[3, 23] + "     |     " + TimeTableName[4, 23] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 23] + "     |     " + TimeTableRoom[1, 23] + "     |     " + TimeTableRoom[2, 23] + "     |     " + TimeTableRoom[3, 23] + "     |     " + TimeTableRoom[4, 23] + "     |     ");
            Console.WriteLine("19:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + TimeTableName[0, 24] + "     |     " + TimeTableName[1, 24] + "     |     " + TimeTableName[2, 24] + "     |     " + TimeTableName[3, 24] + "     |     " + TimeTableName[4, 24] + "     |     ");
            Console.WriteLine("                 " + TimeTableRoom[0, 24] + "     |     " + TimeTableRoom[1, 24] + "     |     " + TimeTableRoom[2, 24] + "     |     " + TimeTableRoom[3, 24] + "     |     " + TimeTableRoom[4, 24] + "     |     ");
            Console.WriteLine("20:00 ------------------------------------------------------------------------------------------------------------------------------\n");
        */

            Console.WriteLine("             월                   화                     수                      목                       금           ");
            Console.WriteLine("08:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 1]).PadLeft(10 - (5 - (TimeTableName[0, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 1]).PadLeft(10 - (5 - (TimeTableName[1, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 1]).PadLeft(10 - (5 - (TimeTableName[2, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 1]).PadLeft(10 - (5 - (TimeTableName[3, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 1]).PadLeft(10 - (5 - (TimeTableName[4, 1].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 1]).PadLeft(10 - (5 - (TimeTableRoom[0, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 1]).PadLeft(10 - (5 - (TimeTableRoom[1, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 1]).PadLeft(10 - (5 - (TimeTableRoom[2, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 1]).PadLeft(10 - (5 - (TimeTableRoom[3, 1].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 1]).PadLeft(10 - (5 - (TimeTableRoom[4, 1].Length / 2))) + "     |     ");
            Console.WriteLine("08:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 2]).PadLeft(10 - (5 - (TimeTableName[0, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 2]).PadLeft(10 - (5 - (TimeTableName[1, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 2]).PadLeft(10 - (5 - (TimeTableName[2, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 2]).PadLeft(10 - (5 - (TimeTableName[3, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 2]).PadLeft(10 - (5 - (TimeTableName[4, 2].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 2]).PadLeft(10 - (5 - (TimeTableRoom[0, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 2]).PadLeft(10 - (5 - (TimeTableRoom[1, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 2]).PadLeft(10 - (5 - (TimeTableRoom[2, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 2]).PadLeft(10 - (5 - (TimeTableRoom[3, 2].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 2]).PadLeft(10 - (5 - (TimeTableRoom[4, 2].Length / 2))) + "     |     ");
            Console.WriteLine("09:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 3]).PadLeft(10 - (5 - (TimeTableName[0, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 3]).PadLeft(10 - (5 - (TimeTableName[1, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 3]).PadLeft(10 - (5 - (TimeTableName[2, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 3]).PadLeft(10 - (5 - (TimeTableName[3, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 3]).PadLeft(10 - (5 - (TimeTableName[4, 3].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 3]).PadLeft(10 - (5 - (TimeTableRoom[0, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 3]).PadLeft(10 - (5 - (TimeTableRoom[1, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 3]).PadLeft(10 - (5 - (TimeTableRoom[2, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 3]).PadLeft(10 - (5 - (TimeTableRoom[3, 3].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 3]).PadLeft(10 - (5 - (TimeTableRoom[4, 3].Length / 2))) + "     |     ");
            Console.WriteLine("09:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 4]).PadLeft(10 - (5 - (TimeTableName[0, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 4]).PadLeft(10 - (5 - (TimeTableName[1, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 4]).PadLeft(10 - (5 - (TimeTableName[2, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 4]).PadLeft(10 - (5 - (TimeTableName[3, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 4]).PadLeft(10 - (5 - (TimeTableName[4, 4].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 4]).PadLeft(10 - (5 - (TimeTableRoom[0, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 4]).PadLeft(10 - (5 - (TimeTableRoom[1, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 4]).PadLeft(10 - (5 - (TimeTableRoom[2, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 4]).PadLeft(10 - (5 - (TimeTableRoom[3, 4].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 4]).PadLeft(10 - (5 - (TimeTableRoom[4, 4].Length / 2))) + "     |     ");
            Console.WriteLine("10:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 5]).PadLeft(10 - (5 - (TimeTableName[0, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 5]).PadLeft(10 - (5 - (TimeTableName[1, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 5]).PadLeft(10 - (5 - (TimeTableName[2, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 5]).PadLeft(10 - (5 - (TimeTableName[3, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 5]).PadLeft(10 - (5 - (TimeTableName[4, 5].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 5]).PadLeft(10 - (5 - (TimeTableRoom[0, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 5]).PadLeft(10 - (5 - (TimeTableRoom[1, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 5]).PadLeft(10 - (5 - (TimeTableRoom[2, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 5]).PadLeft(10 - (5 - (TimeTableRoom[3, 5].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 5]).PadLeft(10 - (5 - (TimeTableRoom[4, 5].Length / 2))) + "     |     ");
            Console.WriteLine("10:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 6]).PadLeft(10 - (5 - (TimeTableName[0, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 6]).PadLeft(10 - (5 - (TimeTableName[1, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 6]).PadLeft(10 - (5 - (TimeTableName[2, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 6]).PadLeft(10 - (5 - (TimeTableName[3, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 6]).PadLeft(10 - (5 - (TimeTableName[4, 6].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 6]).PadLeft(10 - (5 - (TimeTableRoom[0, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 6]).PadLeft(10 - (5 - (TimeTableRoom[1, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 6]).PadLeft(10 - (5 - (TimeTableRoom[2, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 6]).PadLeft(10 - (5 - (TimeTableRoom[3, 6].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 6]).PadLeft(10 - (5 - (TimeTableRoom[4, 6].Length / 2))) + "     |     ");
            Console.WriteLine("11:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 7]).PadLeft(10 - (5 - (TimeTableName[0, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 7]).PadLeft(10 - (5 - (TimeTableName[1, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 7]).PadLeft(10 - (5 - (TimeTableName[2, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 7]).PadLeft(10 - (5 - (TimeTableName[3, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 7]).PadLeft(10 - (5 - (TimeTableName[4, 7].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 7]).PadLeft(10 - (5 - (TimeTableRoom[0, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 7]).PadLeft(10 - (5 - (TimeTableRoom[1, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 7]).PadLeft(10 - (5 - (TimeTableRoom[2, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 7]).PadLeft(10 - (5 - (TimeTableRoom[3, 7].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 7]).PadLeft(10 - (5 - (TimeTableRoom[4, 7].Length / 2))) + "     |     ");
            Console.WriteLine("11:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 8]).PadLeft(10 - (5 - (TimeTableName[0, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 8]).PadLeft(10 - (5 - (TimeTableName[1, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 8]).PadLeft(10 - (5 - (TimeTableName[2, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 8]).PadLeft(10 - (5 - (TimeTableName[3, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 8]).PadLeft(10 - (5 - (TimeTableName[4, 8].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 8]).PadLeft(10 - (5 - (TimeTableRoom[0, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 8]).PadLeft(10 - (5 - (TimeTableRoom[1, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 8]).PadLeft(10 - (5 - (TimeTableRoom[2, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 8]).PadLeft(10 - (5 - (TimeTableRoom[3, 8].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 8]).PadLeft(10 - (5 - (TimeTableRoom[4, 8].Length / 2))) + "     |     ");
            Console.WriteLine("12:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 9]).PadLeft(10 - (5 - (TimeTableName[0, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 9]).PadLeft(10 - (5 - (TimeTableName[1, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 9]).PadLeft(10 - (5 - (TimeTableName[2, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 9]).PadLeft(10 - (5 - (TimeTableName[3, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 9]).PadLeft(10 - (5 - (TimeTableName[4, 9].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 9]).PadLeft(10 - (5 - (TimeTableRoom[0, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 9]).PadLeft(10 - (5 - (TimeTableRoom[1, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 9]).PadLeft(10 - (5 - (TimeTableRoom[2, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 9]).PadLeft(10 - (5 - (TimeTableRoom[3, 9].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 9]).PadLeft(10 - (5 - (TimeTableRoom[4, 9].Length / 2))) + "     |     ");
            Console.WriteLine("12:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 10]).PadLeft(10 - (5 - (TimeTableName[0, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 10]).PadLeft(10 - (5 - (TimeTableName[1, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 10]).PadLeft(10 - (5 - (TimeTableName[2, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 10]).PadLeft(10 - (5 - (TimeTableName[3, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 10]).PadLeft(10 - (5 - (TimeTableName[4, 10].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 10]).PadLeft(10 - (5 - (TimeTableRoom[0, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 10]).PadLeft(10 - (5 - (TimeTableRoom[1, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 10]).PadLeft(10 - (5 - (TimeTableRoom[2, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 10]).PadLeft(10 - (5 - (TimeTableRoom[3, 10].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 10]).PadLeft(10 - (5 - (TimeTableRoom[4, 10].Length / 2))) + "     |     ");
            Console.WriteLine("13:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 11]).PadLeft(10 - (5 - (TimeTableName[0, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 11]).PadLeft(10 - (5 - (TimeTableName[1, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 11]).PadLeft(10 - (5 - (TimeTableName[2, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 11]).PadLeft(10 - (5 - (TimeTableName[3, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 11]).PadLeft(10 - (5 - (TimeTableName[4, 11].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 11]).PadLeft(10 - (5 - (TimeTableRoom[0, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 11]).PadLeft(10 - (5 - (TimeTableRoom[1, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 11]).PadLeft(10 - (5 - (TimeTableRoom[2, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 11]).PadLeft(10 - (5 - (TimeTableRoom[3, 11].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 11]).PadLeft(10 - (5 - (TimeTableRoom[4, 11].Length / 2))) + "     |     ");
            Console.WriteLine("13:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 12]).PadLeft(10 - (5 - (TimeTableName[0, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 12]).PadLeft(10 - (5 - (TimeTableName[1, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 12]).PadLeft(10 - (5 - (TimeTableName[2, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 12]).PadLeft(10 - (5 - (TimeTableName[3, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 12]).PadLeft(10 - (5 - (TimeTableName[4, 12].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 12]).PadLeft(10 - (5 - (TimeTableRoom[0, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 12]).PadLeft(10 - (5 - (TimeTableRoom[1, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 12]).PadLeft(10 - (5 - (TimeTableRoom[2, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 12]).PadLeft(10 - (5 - (TimeTableRoom[3, 12].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 12]).PadLeft(10 - (5 - (TimeTableRoom[4, 12].Length / 2))) + "     |     ");
            Console.WriteLine("14:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 13]).PadLeft(10 - (5 - (TimeTableName[0, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 13]).PadLeft(10 - (5 - (TimeTableName[1, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 13]).PadLeft(10 - (5 - (TimeTableName[2, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 13]).PadLeft(10 - (5 - (TimeTableName[3, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 13]).PadLeft(10 - (5 - (TimeTableName[4, 13].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 13]).PadLeft(10 - (5 - (TimeTableRoom[0, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 13]).PadLeft(10 - (5 - (TimeTableRoom[1, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 13]).PadLeft(10 - (5 - (TimeTableRoom[2, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 13]).PadLeft(10 - (5 - (TimeTableRoom[3, 13].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 13]).PadLeft(10 - (5 - (TimeTableRoom[4, 13].Length / 2))) + "     |     ");
            Console.WriteLine("14:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 14]).PadLeft(10 - (5 - (TimeTableName[0, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 14]).PadLeft(10 - (5 - (TimeTableName[1, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 14]).PadLeft(10 - (5 - (TimeTableName[2, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 14]).PadLeft(10 - (5 - (TimeTableName[3, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 14]).PadLeft(10 - (5 - (TimeTableName[4, 14].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 14]).PadLeft(10 - (5 - (TimeTableRoom[0, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 14]).PadLeft(10 - (5 - (TimeTableRoom[1, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 14]).PadLeft(10 - (5 - (TimeTableRoom[2, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 14]).PadLeft(10 - (5 - (TimeTableRoom[3, 14].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 14]).PadLeft(10 - (5 - (TimeTableRoom[4, 14].Length / 2))) + "     |     ");
            Console.WriteLine("15:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 15]).PadLeft(10 - (5 - (TimeTableName[0, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 15]).PadLeft(10 - (5 - (TimeTableName[1, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 15]).PadLeft(10 - (5 - (TimeTableName[2, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 15]).PadLeft(10 - (5 - (TimeTableName[3, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 15]).PadLeft(10 - (5 - (TimeTableName[4, 15].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 15]).PadLeft(10 - (5 - (TimeTableRoom[0, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 15]).PadLeft(10 - (5 - (TimeTableRoom[1, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 15]).PadLeft(10 - (5 - (TimeTableRoom[2, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 15]).PadLeft(10 - (5 - (TimeTableRoom[3, 15].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 15]).PadLeft(10 - (5 - (TimeTableRoom[4, 15].Length / 2))) + "     |     ");
            Console.WriteLine("15:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 16]).PadLeft(10 - (5 - (TimeTableName[0, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 16]).PadLeft(10 - (5 - (TimeTableName[1, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 16]).PadLeft(10 - (5 - (TimeTableName[2, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 16]).PadLeft(10 - (5 - (TimeTableName[3, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 16]).PadLeft(10 - (5 - (TimeTableName[4, 16].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 16]).PadLeft(10 - (5 - (TimeTableRoom[0, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 16]).PadLeft(10 - (5 - (TimeTableRoom[1, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 16]).PadLeft(10 - (5 - (TimeTableRoom[2, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 16]).PadLeft(10 - (5 - (TimeTableRoom[3, 16].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 16]).PadLeft(10 - (5 - (TimeTableRoom[4, 16].Length / 2))) + "     |     ");
            Console.WriteLine("16:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 17]).PadLeft(10 - (5 - (TimeTableName[0, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 17]).PadLeft(10 - (5 - (TimeTableName[1, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 17]).PadLeft(10 - (5 - (TimeTableName[2, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 17]).PadLeft(10 - (5 - (TimeTableName[3, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 17]).PadLeft(10 - (5 - (TimeTableName[4, 17].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 17]).PadLeft(10 - (5 - (TimeTableRoom[0, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 17]).PadLeft(10 - (5 - (TimeTableRoom[1, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 17]).PadLeft(10 - (5 - (TimeTableRoom[2, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 17]).PadLeft(10 - (5 - (TimeTableRoom[3, 17].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 17]).PadLeft(10 - (5 - (TimeTableRoom[4, 17].Length / 2))) + "     |     ");
            Console.WriteLine("16:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 18]).PadLeft(10 - (5 - (TimeTableName[0, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 18]).PadLeft(10 - (5 - (TimeTableName[1, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 18]).PadLeft(10 - (5 - (TimeTableName[2, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 18]).PadLeft(10 - (5 - (TimeTableName[3, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 18]).PadLeft(10 - (5 - (TimeTableName[4, 18].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 18]).PadLeft(10 - (5 - (TimeTableRoom[0, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 18]).PadLeft(10 - (5 - (TimeTableRoom[1, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 18]).PadLeft(10 - (5 - (TimeTableRoom[2, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 18]).PadLeft(10 - (5 - (TimeTableRoom[3, 18].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 18]).PadLeft(10 - (5 - (TimeTableRoom[4, 18].Length / 2))) + "     |     ");
            Console.WriteLine("17:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 19]).PadLeft(10 - (5 - (TimeTableName[0, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 19]).PadLeft(10 - (5 - (TimeTableName[1, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 19]).PadLeft(10 - (5 - (TimeTableName[2, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 19]).PadLeft(10 - (5 - (TimeTableName[3, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 19]).PadLeft(10 - (5 - (TimeTableName[4, 19].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 19]).PadLeft(10 - (5 - (TimeTableRoom[0, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 19]).PadLeft(10 - (5 - (TimeTableRoom[1, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 19]).PadLeft(10 - (5 - (TimeTableRoom[2, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 19]).PadLeft(10 - (5 - (TimeTableRoom[3, 19].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 19]).PadLeft(10 - (5 - (TimeTableRoom[4, 19].Length / 2))) + "     |     ");
            Console.WriteLine("17:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 20]).PadLeft(10 - (5 - (TimeTableName[0, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 20]).PadLeft(10 - (5 - (TimeTableName[1, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 20]).PadLeft(10 - (5 - (TimeTableName[2, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 20]).PadLeft(10 - (5 - (TimeTableName[3, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 20]).PadLeft(10 - (5 - (TimeTableName[4, 20].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 20]).PadLeft(10 - (5 - (TimeTableRoom[0, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 20]).PadLeft(10 - (5 - (TimeTableRoom[1, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 20]).PadLeft(10 - (5 - (TimeTableRoom[2, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 20]).PadLeft(10 - (5 - (TimeTableRoom[3, 20].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 20]).PadLeft(10 - (5 - (TimeTableRoom[4, 20].Length / 2))) + "     |     ");
            Console.WriteLine("18:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 21]).PadLeft(10 - (5 - (TimeTableName[0, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 21]).PadLeft(10 - (5 - (TimeTableName[1, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 21]).PadLeft(10 - (5 - (TimeTableName[2, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 21]).PadLeft(10 - (5 - (TimeTableName[3, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 21]).PadLeft(10 - (5 - (TimeTableName[4, 21].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 21]).PadLeft(10 - (5 - (TimeTableRoom[0, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 21]).PadLeft(10 - (5 - (TimeTableRoom[1, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 21]).PadLeft(10 - (5 - (TimeTableRoom[2, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 21]).PadLeft(10 - (5 - (TimeTableRoom[3, 21].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 21]).PadLeft(10 - (5 - (TimeTableRoom[4, 21].Length / 2))) + "     |     ");
            Console.WriteLine("18:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 22]).PadLeft(10 - (5 - (TimeTableName[0, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 22]).PadLeft(10 - (5 - (TimeTableName[1, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 22]).PadLeft(10 - (5 - (TimeTableName[2, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 22]).PadLeft(10 - (5 - (TimeTableName[3, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 22]).PadLeft(10 - (5 - (TimeTableName[4, 22].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 22]).PadLeft(10 - (5 - (TimeTableRoom[0, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 22]).PadLeft(10 - (5 - (TimeTableRoom[1, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 22]).PadLeft(10 - (5 - (TimeTableRoom[2, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 22]).PadLeft(10 - (5 - (TimeTableRoom[3, 22].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 22]).PadLeft(10 - (5 - (TimeTableRoom[4, 22].Length / 2))) + "     |     ");
            Console.WriteLine("19:00 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 23]).PadLeft(10 - (5 - (TimeTableName[0, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 23]).PadLeft(10 - (5 - (TimeTableName[1, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 23]).PadLeft(10 - (5 - (TimeTableName[2, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 23]).PadLeft(10 - (5 - (TimeTableName[3, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 23]).PadLeft(10 - (5 - (TimeTableName[4, 23].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 23]).PadLeft(10 - (5 - (TimeTableRoom[0, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 23]).PadLeft(10 - (5 - (TimeTableRoom[1, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 23]).PadLeft(10 - (5 - (TimeTableRoom[2, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 23]).PadLeft(10 - (5 - (TimeTableRoom[3, 23].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 23]).PadLeft(10 - (5 - (TimeTableRoom[4, 23].Length / 2))) + "     |     ");
            Console.WriteLine("19:30 ------------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableName[0, 24]).PadLeft(10 - (5 - (TimeTableName[0, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[1, 24]).PadLeft(10 - (5 - (TimeTableName[1, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[2, 24]).PadLeft(10 - (5 - (TimeTableName[2, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[3, 24]).PadLeft(10 - (5 - (TimeTableName[3, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableName[4, 24]).PadLeft(10 - (5 - (TimeTableName[4, 24].Length / 2))) + "     |     ");
            Console.WriteLine("                 " + String.Format("{0}", TimeTableRoom[0, 24]).PadLeft(10 - (5 - (TimeTableRoom[0, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[1, 24]).PadLeft(10 - (5 - (TimeTableRoom[1, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[2, 24]).PadLeft(10 - (5 - (TimeTableRoom[2, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[3, 24]).PadLeft(10 - (5 - (TimeTableRoom[3, 24].Length / 2))) + "     |     " + String.Format("{0}", TimeTableRoom[4, 24]).PadLeft(10 - (5 - (TimeTableRoom[4, 24].Length / 2))) + "     |     ");
            Console.WriteLine("20:00 ------------------------------------------------------------------------------------------------------------------------------\n");
        }
    }
}
