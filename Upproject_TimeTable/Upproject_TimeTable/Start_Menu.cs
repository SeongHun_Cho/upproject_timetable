﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Upproject_TimeTable
{
    class Start_Menu
    {

        bool repeat1 = true; //while 반복문을 쓰기 위한 변수는 어떤 방식으로 선언을 하는게 좋을까???
        public Array ExcelData = new Array[300];
        public List<NumberClass> ApplySubject = new List<NumberClass>();
        public List<NumberClass> InterestSubject = new List<NumberClass>();
        string[,] TimeTableValue = new string[5, 25];
        ApplyMenu ApplyMenu = new ApplyMenu();
        InterestMenu InterestMenu = new InterestMenu();
        MyTimeTable MyTimeTable = new MyTimeTable();


        //생성자해서 엑셀불러오는 함수를 한번실행한다
        public Start_Menu()
        {
            Excel.Application ExcelApp = new Excel.Application();
            Excel.Workbook workbook = ExcelApp.Workbooks.Open(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\timetable2.xlsx");
            Excel.Sheets sheets = workbook.Sheets;
            Excel.Worksheet worksheet = sheets["sheet1"] as Excel.Worksheet;
            Excel.Range cellRange = worksheet.get_Range("A1", "L180") as Excel.Range;  //배열로 엑셀 데이터 값을 넣음
            ExcelData = cellRange.Cells.Value2;

            workbook.Close();
            ExcelApp.Quit();
        }

       

        //시작메뉴
        public void Menu() //시작메뉴
        {
            int MenuSelect;
            while (repeat1) { 
            Console.Write("                           T I M E T A B L E \n\n\n\n\n");
            Console.Write("           1. 수강신청\n\n");
            Console.Write("           2. 관심과목\n\n");
            Console.Write("           3. 나의 시간표\n\n");
            Console.Write("           4. 종 료\n\n");
            Console.Write("         원하는 번호를 고르시오  :");

            int.TryParse(Console.ReadLine(), out MenuSelect);
                switch (MenuSelect)
                {
                    case 1:
                        //수강신청 메뉴
                        Console.Clear();
                        ApplySubject = ApplyMenu.Apply(ApplySubject,ExcelData,InterestSubject);
                        break;
                    case 2:
                        //관심과목 메뉴
                        Console.Clear();
                        InterestSubject = InterestMenu.Interest(InterestSubject,ExcelData);
                        break;
                    case 3:
                        //나의 시간표
                        Console.Clear();
                        MyTimeTable.TimeTable(ApplySubject,ExcelData);
                        MyTimeTable.TimeTableBackGround();
                        Console.Write("엑셀로 저장하시겠습니까? 1.Yes 2.No\n");
                        int.TryParse(Console.ReadLine(), out int Save);
                        if (Save == 1)
                        {
                            Excel.Application ExcelApp = new Excel.Application();
                            Excel.Workbook workbook = ExcelApp.Workbooks.Add();
                            Excel.Worksheet workSheet = workbook.Worksheets.get_Item(1) as Excel.Worksheet;
                            for (int val = 1; val < 13; val++) {
                                workSheet.Cells[1, val] = ExcelData.GetValue(1, val);
                            }

                            for(int sheetvalue = 0; sheetvalue < ApplySubject.Count(); sheetvalue++)
                            {
                                workSheet.Cells[sheetvalue + 2, 1] = Convert.ToString(ApplySubject[sheetvalue].NO);
                                workSheet.Cells[sheetvalue + 2, 2] = Convert.ToString(ApplySubject[sheetvalue].Subject);
                                workSheet.Cells[sheetvalue + 2, 3] = Convert.ToString(ApplySubject[sheetvalue].ClassNumber);
                                workSheet.Cells[sheetvalue + 2, 4] = Convert.ToString(ApplySubject[sheetvalue].divide);
                                workSheet.Cells[sheetvalue + 2, 5] = Convert.ToString(ApplySubject[sheetvalue].ClassName);
                                workSheet.Cells[sheetvalue + 2, 6] = Convert.ToString(ApplySubject[sheetvalue].division);
                                workSheet.Cells[sheetvalue + 2, 7] = Convert.ToString(ApplySubject[sheetvalue].grade);
                                workSheet.Cells[sheetvalue + 2, 8] = Convert.ToString(ApplySubject[sheetvalue].credit);
                                workSheet.Cells[sheetvalue + 2, 9] = Convert.ToString(ApplySubject[sheetvalue].time);
                                workSheet.Cells[sheetvalue + 2, 10] = Convert.ToString(ApplySubject[sheetvalue].room);
                                workSheet.Cells[sheetvalue + 2, 11] = Convert.ToString(ApplySubject[sheetvalue].professor);
                                workSheet.Cells[sheetvalue + 2, 12] = Convert.ToString(ApplySubject[sheetvalue].language);
                            }
                            
                            workSheet.Columns.EntireColumn.AutoFit();  //너비 자동맞춤
                            workbook.SaveAs(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\new.xls", Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                            
                            workbook.Close();
                            ExcelApp.Quit();
                        }

                        Console.Clear();
                        break;
                    case 4:
                        //종 료(프로그램 종료)
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

       
       

       
       

        

      


    }
}
